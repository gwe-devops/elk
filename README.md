#ExpressJS EFK (Elasticsearch-FluentD-Kibana)

#1. hosts file
``> sudo vi /etc/hosts``  
Add an entry as below  
``127.0.0.1 elasticsearch``  
``127.0.0.1 kibana``


#2. Start the services
``> cd expressjs-webapi``  
``> docker-compose build``  
``> docker-compose up``

#3. Access Kibaba
``http://localhost:5601``  
First kibana setup refer to http://blog.logicwind.com/efk-setup/

#4. References
https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html
https://www.elastic.co/guide/en/kibana/current/docker.html

