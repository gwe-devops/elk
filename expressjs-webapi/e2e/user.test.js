
require("dotenv").config();
const request = require("supertest");
const app = require("../src/app");
const mongoose = require('mongoose');
let tmp_id = null;

describe("/api/users", () => {
    test("GET /api/users should returns users array", async (done) => {
        const response = await request(app).get("/api/users");
        expect(response.statusCode).toBe(200);
        expect(response.body.status).toBe("success");
        expect(response.body.data.length).toBeGreaterThanOrEqual(0);
        done();
    });

    test("POST /api/users should create user", async (done) => {
        const response = await request(app).post("/api/users").send({
            "first_name": "jest_test",
            "last_name": "jest_test",
            "email": "jest_test@gwe.com"
        });
        expect(response.statusCode).toBe(200);
        expect(response.body.status).toBe("success");
        tmp_id = response.body.data.id;
        done()
    });
    test("GET /api/users should get user by id:" + tmp_id, async (done) => {
        const response = await request(app).get("/api/users/" + tmp_id);
        expect(response.statusCode).toBe(200);
        expect(response.body.status).toBe("success");
        done();
    });

    test("DELETE /api/users should delete user by id:" + tmp_id, async (done) => {
        const response = await request(app).delete("/api/users/" + tmp_id);
        expect(response.statusCode).toBe(200);
        expect(response.body.status).toBe("success");
        done();
    });
    afterAll(async () => {
        await mongoose.connection.close()
    })
});