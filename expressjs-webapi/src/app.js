const express = require("express");
const express_http_context = require('express-http-context');
const router = require("./router");
const uuid = require("uuid").v1;
const { request_logger, error_logger, response_logger } = require("./logger");
const app = new express();

app.use(express_http_context.middleware);
app.use((req, res, next)=> {
    req.request_id = uuid();
    express_http_context.set('request_id', req.request_id);
    next();
});
app.use(request_logger);
app.use(error_logger);
app.use(response_logger);
app.use(express.json());
app.use("/api", router);

module.exports = app;
