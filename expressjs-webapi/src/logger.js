const winston = require('winston');
const mung = require('express-mung');
const express_http_context = require('express-http-context');
const {combine, timestamp, printf} = winston.format;
const consoleLog = new winston.transports.Console()

const logger = winston.createLogger({
    level: 'debug',
    transports: [
        new winston.transports.Console()
    ],
    format: combine(
        timestamp(),
        printf(info => {
            return JSON.stringify({
                request_id: express_http_context.get('request_id'),
                timestamp: info.timestamp,
                level: info.level,
                message:info.message
            });
        })),
    exitOnError: false,
});

function createRequestLogger(transports) {
    const requestLogger = winston.createLogger({
        level: 'info',
        format: getRequestLogFormatter(),
        transports: transports
    })

    return function logRequest(req, res, next) {
        requestLogger.info({req, res});
        next()
    }
}

function createResponseLogger(transports) {
    const responseLogger = winston.createLogger({
        level: 'info',
        format: getResponseLogFormatter(),
        transports: transports
    })

    return mung.json(
        function transform(body, req, res) {
            responseLogger.info({body: body.server, conversation_id: express_http_context.get('request_id') || req.conversation_id});
            return body;
        }
    );
}

function createErrorLogger(transports) {
    const errLogger = winston.createLogger({
        level: 'error',
        transports: transports
    })

    return function logError(err, req, res, next) {
        errLogger.error({err, req, res})
        next()
    }
}

function getRequestLogFormatter() {
    return combine(
        timestamp(),
        printf(info => {
            const {req, res} = info.message;
            return JSON.stringify({
                request_id: express_http_context.get('request_id') || req.request_id,
                type:"request",
                timestamp: info.timestamp,
                level: info.level,
                message:`${req.hostname}${req.port || ''}${req.originalUrl}`
            });
        })
    );
}

function getResponseLogFormatter() {
    return combine(
        timestamp(),
        printf(info => {
            const{ request_id, body } = info.message;
            return JSON.stringify({
                request_id: express_http_context.get('request_id') || request_id,
                type:"response",
                timestamp: info.timestamp,
                level: info.level,
                message:body
            });
        })
    );
}



module.exports = {
    request_logger: createRequestLogger([consoleLog]),
    response_logger: createResponseLogger([consoleLog]),
    error_logger: createErrorLogger([consoleLog]),
    logger
}
