const express = require("express");
const {logger} = require("./logger");
let router = express.Router();

router.get("/", function (req, res) {
  logger.info("Sending message");
  res.json({
    server: {
      time: Date.now(),
      name: process.env.APP_NAME,
      port: process.env.APP_PORT,
    },
  });
});

module.exports = router;
