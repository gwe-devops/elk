const yargs = require("yargs").argv;
const defaults = {
  port: 7001,
  name: "app-1",
  http: 1,
};

let env = yargs.env === undefined || yargs.env === null ? {} : require("./" + yargs.env.toLowerCase());

//docker run time value
if (process.env.APP_ENV) {
  env = require("./" + process.env.APP_ENV.toLowerCase());
}

let settings = { ...defaults, ...env };

if (process.env.APP_PORT) {
  settings.port = process.env.APP_PORT;
} else if (yargs.port) {
  settings.port = yargs.port;
}

if (process.env.APP_NAME) {
  settings.name = process.env.APP_NAME;
} else if (yargs.name) {
  settings.name = yargs.name;
}

for (const k in settings) {
  process.env[`APP_${k.toUpperCase()}`] = settings[k];
}
module.exports = settings;
