docker rm -f docker-image-express-api-01
docker image rm docker-image-express-api:latest
docker build -f Dockerfile -t docker-image-express-api:latest --rm .
docker run -d \
--name=docker-image-express-api-01 \
-p 7001:7001 \
-e APP_PORT=7001 \
-e APP_NAME=api-01 \
-e APP_ENV=dev \
docker-image-express-api:latest
docker logs docker-image-express-api-01 -n=100 -f
