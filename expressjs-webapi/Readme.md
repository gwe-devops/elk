# docker build command

`docker build -t docker-image-express-api`

# docker run with run time arguments

`docker run -d --name=docker-image-express-api-01 -p 7001:3000 -e APP_PORT 3000 -e APP_NAME =pi-01 docker-image-express-api`
